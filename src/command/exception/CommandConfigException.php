<?php
/**
 * This file is part of monda-worker.
 *
 * @contact  mondagroup_php@163.com
 *
 */
namespace framework\command\exception;

class CommandConfigException extends \RuntimeException
{
}
