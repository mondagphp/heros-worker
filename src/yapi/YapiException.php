<?php
/**
 * This file is part of monda-worker.
 *
 * @contact  mondagroup_php@163.com
 *
 */
namespace framework\yapi;

class YapiException extends \RuntimeException
{
}
